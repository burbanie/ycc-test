	# Use a base image with Java 17
FROM openjdk:17-jdk-slim-buster as build
COPY . /home/gradle/src
WORKDIR /home/gradle/src
RUN ./gradlew build


FROM openjdk:17-jdk-slim-buster

# Set environment variables
ENV APP_HOME=/app \
    JAVA_OPTS=""

# Set the working directory
WORKDIR $APP_HOME

# Copy the JAR file into the container
COPY --from=build /home/gradle/src/build/libs/*-SNAPSHOT.jar $APP_HOME/app.jar

EXPOSE 8080

# Set the entrypoint and command to run the Spring Boot application
ENTRYPOINT ["java", "-jar", "app.jar"]
CMD ["$JAVA_OPTS"]

