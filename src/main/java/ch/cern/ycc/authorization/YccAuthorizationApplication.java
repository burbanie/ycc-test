package ch.cern.ycc.authorization;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YccAuthorizationApplication {

	public static void main(String[] args) {
		SpringApplication.run(YccAuthorizationApplication.class, args);
	}

}
